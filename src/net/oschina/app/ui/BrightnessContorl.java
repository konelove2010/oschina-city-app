package net.oschina.app.ui;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import net.oschina.app.R;

public class BrightnessContorl extends BaseActivity {
	private SeekBar bright;
	private RelativeLayout relativeLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.brightness_control);
		initView();
	}

	public BrightnessContorl() {
	}

	private void initView() {
		bright = (SeekBar) findViewById(R.id.brightness_control);
		relativeLayout = (RelativeLayout) findViewById(R.id.brightness_control_layout);
		relativeLayout.getBackground().setAlpha(80);
	}
}
