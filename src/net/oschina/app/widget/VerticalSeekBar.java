package net.oschina.app.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class VerticalSeekBar extends SeekBar {

	private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = null;

	private VerticalSeekBar.onTouthPressDownListener onTouthPressDownListener = null;

	private VerticalSeekBar.onTouthPressUpListener onTouthPressUpListener = null;

	public VerticalSeekBar.onTouthPressDownListener getOnTouthPressDownListener() {
		return onTouthPressDownListener;
	}

	public void setOnTouthPressDownListener(
			VerticalSeekBar.onTouthPressDownListener onTouthPressDownListener) {
		this.onTouthPressDownListener = onTouthPressDownListener;
	}

	public VerticalSeekBar.onTouthPressUpListener getOnTouthPressUpListener() {
		return onTouthPressUpListener;
	}

	public void setOnTouthPressUpListener(
			VerticalSeekBar.onTouthPressUpListener onTouthPressUpListener) {
		this.onTouthPressUpListener = onTouthPressUpListener;
	}

	public static interface onTouthPressDownListener {

		public void onPressDown(SeekBar bar, int progress, MotionEvent event);
	}

	public static interface onTouthPressUpListener {

		public void onPressUp(SeekBar bar, int progress, MotionEvent event);
	}

	public SeekBar.OnSeekBarChangeListener getOnSeekBarChangeListener() {
		return onSeekBarChangeListener;
	}

	public void setOnSeekBarChangeListener(
			SeekBar.OnSeekBarChangeListener onSeekBarChangeListener) {
		this.onSeekBarChangeListener = onSeekBarChangeListener;
	}

	public VerticalSeekBar(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public VerticalSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(h, w, oldh, oldw);
	}

	@Override
	protected synchronized void onMeasure(int widthMeasureSpec,
			int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, widthMeasureSpec);
		setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		canvas.rotate(-90);
		canvas.translate(-getHeight(), 0);
		super.onDraw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!isEnabled()) {
			return false;
		}

		switch (event.getAction()) {
		case MotionEvent.ACTION_POINTER_DOWN:
			if (null != this.onTouthPressDownListener)
				this.onTouthPressDownListener.onPressDown(this,
						this.getProgress(), event);
			break;
		case MotionEvent.ACTION_POINTER_UP:
			if (null != this.onTouthPressDownListener)
				this.onTouthPressDownListener.onPressDown(this,
						this.getProgress(), event);
			break;
		case MotionEvent.ACTION_DOWN:
			setProgress(getMax()
					+ (int) (getMax() * event.getY() / getHeight()));
			onSizeChanged(getWidth(), getHeight(), 0, 0);
			this.onSeekBarChangeListener.onProgressChanged(this,
					this.getProgress(), false);
		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
			setProgress(getMax()
					- (int) (getMax() * event.getY() / getHeight()));
			onSizeChanged(getWidth(), getHeight(), 0, 0);
			this.onSeekBarChangeListener.onProgressChanged(this,
					this.getProgress(), false);
			break;

		case MotionEvent.ACTION_CANCEL:
			break;
		}

		return true;
	}
}
